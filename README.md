# Introduction #

This README file was created to give you instructions on how to execute the following python programs: 
	diffusion.py
	xray.py
You will be able to use these programs to interpret data relating to diffusivity, and xray data.



##Instructions for running diffusion.py and xray.py on your computer's bash terminal##

###STEP 1##

First ensure that your diffusion.py, xray.py, diffusivity.txt and xray.txt are all in the same directory. All of these files must be located in the same directory or the programs will not be able to find the txt files. The directory you choose will not matter.

###STEP 2##

Then navigate to that directory using the bash terminal.

###STEP 3

Run the following code for diffusion.py

		--> python diffusion.py diffusivity.txt <--
		
Run the following code for xray.py

		--> python xray.py xray.txt <--
		
###STEP 4

You're done! Your data should be printed out after inputting the last command.

##Instructions for running diffusion.py on CodeLab##


###STEP 1

Navigate to the following website:

	http://codelab.boisestate.edu/

###STEP 2

In the top left corner of the website click "upload". Select the diffusion.py file, and the diffusivity.txt file from you local directory *Hint* this is where ever you have it saved.
Then click the blue upload button next to each file in the browser.

###STEP 3

Click and open diffusion.py in the browser. Copy the entire code by highlighting the entire code, right-click and select copy or hit cmd-c or ctrl c to copy.

###STEP 4

Select the main "Jupiter" Icon at the top of the page to navigate back to the codelab homepage. Next select the "new" button on the top right corner, and from the drop down menu, select "Python 3". This will create a new python 3 notebook for you to run your code.

###STEP 5

Next the "In [ ]:", in the text box paste the code you previously copied.

###STEP 6

Inside your code, navigate to this line,
			--> data = np.loadtxt(sys.argv[1]) <--
change this code so it reads like this
			--> data = np.loadtxt('diffusivity.txt') <--

###STEP 7

Once you have made the change in step 6, on your keyboard hit "shift - enter".

###STEP 8

You are done! The code should run your program and give you computational data you want.



##Instructions for running xray.py on CodeLab##

###STEP 1

Repeat Step 1-5 of the previous instructions but instead of uploading diffusion.py and diffusivity.txt, upload xray.py and xray.txt.

###STEP 2

In the code, navigate to where the code looks like this,

		-->data = np.loadtxt(sys.argv[1], usecols=(1,2))<--

change it so it looks like this,

		-->data = np.loadtxt('xray.txt', usecols=(1,2))<--

###STEP 3

In codelab, hit "shift - enter" to run the code.

###STEP 4

You're done! The code should run your program and give you computational data you want.



##Contact##
If you need any help understanding or on running the code, you can use the contributors.txt to contact any of the authors by email or phone.
