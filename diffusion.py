# This is the diffusion program.

from scipy import stats

import numpy as np # This code creates dimensional arrays.

import matplotlib.pyplot as plt # This code installs the necessary program to create plots from data

import sys # This imports sys library that will allows us to call on the different arguments

data = np.loadtxt(sys.argv[1]) # This code installs the necessary program to create plots from data


x = 1/(data[:,1])
y = np.log(data[:,0])

cof = np.polyfit(x, y, 1)

Ea =(-8.6173303*10**-5)*cof[0] # This code defines activation energy  

print(Ea)

slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)

print(x)
print(y)

plt.plot(x,y) # This plots x and y on a graph

print(slope)
print(intercept)

E = slope*(-8.617E-5) # E represents the activation energy
r = 'Activation Energy = '+repr(E)+' eV'
A = np.exp(intercept)
g = 'Diffusion Coefficient = '+repr(A)+' (cm^2)/s'
print(r)
print(g)
q = r_value**2 # This r^2 value shows how linear the line is by how close it is to 1
h = 'R-squared value = '+repr(q) #This adds a phrase in front of the value of q
print(h)
