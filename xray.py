# Repository for xray code
import scipy.constants
import math
from scipy import constants
from scipy import stats
# scipy is library used for scientific computing


import numpy as np # This is an extension of python with various scientific graphing methods

import matplotlib.pyplot as plt # Allows us to plot our data

import sys #This imports sys library that will allows us to call on the different arguments

data = np.loadtxt(sys.argv[1], usecols=(1,2)) # Program that allows us to organize the data into a plot


y=np.sqrt((scipy.constants.c)/(data[:,1]*(1e-9))) #This defines our y variable which is the square root of frequency
x=(data[:,0]) #This defines our x variable which is the atomic number of the element
#scipy.constants.c is the value for the speed of light in m/s
y=np.sqrt((scipy.constants.c)/(data[:,1])) # This defines our y variable which is the square root of frequency
x=(data[:,0]) # This defines our x variable which is the atomic number of the element
# scipy.constants.c is the value for the speed of light in m/s

print (x) # This prints the atomic number so we can see the value
print (y) # This prints the square root of frequency to we can see the value

slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
plt.plot(x,y) # This plots x and y on a graph


B = slope
j = 'B = '+repr(B)
C =-(intercept/B)

m = 'C = '+repr(C)
print(j) #This prints the constant "B"
print(m) #This prints the constant "C"
d = r_value**2
g = 'R-squard value = '+repr(d)
#These three lines define the constants

print(scipy.constants.c) #This prints the speed of light in m/s
print(B) #This prints the slope
print(C) #This prints negative of the intercept divided by slope

m = 'C = '+repr(C) #This renames the constant 'C' to 'm'
print(j) # This prints the constant "B"
print(m) # This prints the constant "C"
d = r_value**2 # The r^2 value shows how linear the line is, closer to one is more linear
g = 'R-squard value = '+repr(d) 
print(g)


#indexer
i = 0


#frequency list 
f = (scipy.constants.c)/(data[:,1]*(1e-9))

#List with Planks number values
PlanckList = []

#ratio list
RatioList = []

plancks= 4.135667662e-15

#Finding how good model is 

while (i< len(x)):
        PlanckList.append((13.6*3*((x[i]-1)**2))/(4*f[i]))
        print("Plancks value # " + str(i) + " is: \n " + str(PlanckList[i]))
        RatioList.append(plancks/PlanckList[i])
        print("\nRatio # " + str(i) + " is: \n " + str(RatioList[i])+ "\n")
        i = i+1
print("____________________________________________________")
print("\nAverage value of Plancks Constant Calculated: " + str(sum(PlanckList)/float(len(RatioList))))
print("\nRatio between calculated Plancks constant and actual Plancks constant: " + str(sum(RatioList)/ float(len(RatioList))))

